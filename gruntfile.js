module.exports = function (grunt) {
  grunt.initConfig({
    uglify: {
        custom: {
            files: {
                'js/multistep-premium-modified.min.js': ['js/multistep-premium-modified.js'],
                'js/js.cookie.min.js': ['js/js.cookie.js'],
            },
        },
    },
  });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', 'uglify');
};
