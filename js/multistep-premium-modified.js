/* YITH WooCommerce Multi Step Checkout */
(function($){
    //yith_wcms.dom element are documented in /includes\class.yith-multistep-checkout-frontend-premium.php:187
    var $body               = $('body'),
        login               = $(yith_wcms.dom.login),
        billing             = $(yith_wcms.dom.billing),
        shipping            = $(yith_wcms.dom.shipping),
        offers              = $(yith_wcms.dom.offers),
        order               = $(yith_wcms.dom.order),
        payment             = $(yith_wcms.dom.payment),
        form_actions        = $(yith_wcms.dom.form_actions),
        coupon              = $(yith_wcms.dom.coupon),
        create_account      = $(yith_wcms.dom.create_account),
        create_account_wrapper  = $(yith_wcms.dom.create_account_wrapper),
        steps               = new Array(login, billing, shipping, offers, order, payment),
        is_user_logged_in   = $body.hasClass('logged-in'),
        cookie = {
            form: 'yith_wcms_checkout_form',
            step: 'yith_wcms_checkout_current_step'
        };

    $body.on('updated_checkout yith_wcms_myaccount_order_pay', function(e){
        if (e.type == 'updated_checkout') {
            steps[5] = $(yith_wcms.dom.payment);
        }

        var current_step = form_actions.data('step');
        if (current_step == 5) {
            $(yith_wcms.dom.payment).show();
        }

        $body.trigger('yith_wcms_updated_checkout');
    });

    if ($body.hasClass('woocommerce-order-pay')) {
        $body.trigger('yith_wcms_myaccount_order_pay');
    }

    // make WDUFU required
    $(document).ready(function(){
        $('.wdufuoptions_dropdown').addClass('validate-required');
        $('label[for="wheredidyoufindus"]').append(': <abbr class="required" title="required">*</abbr>');
    });

    // enable select2
    $body.on('yith_wcms_select2', function (event) {
        if ($().select2) {
            var wc_country_select_select2 = function () {
                $('select.country_select, select.state_select, select#wheredidyoufindus').each(function () {
                    var select2_args = {
                        placeholder      : $(this).attr('placeholder'),
                        placeholderOption: 'first',
                        width            : '100%'
                    };

                    $(this).select2(select2_args);
                });
            };

            wc_country_select_select2();

            $body.bind('country_to_state_changed', function () {
                wc_country_select_select2();
            });
        }
    });

    if (yith_wcms.wc_shipping_multiple != 1) {
        $body.trigger('yith_wcms_select2');
    }

    // highlight billing tab if user is logged in
    if (typeof(is_user_logged_in) != undefined && is_user_logged_in == true) {
        $('.yith-wcms-pro ' + yith_wcms.dom.checkout_timeline + ' .billing').addClass('active');
    }

    // handle headers
    $('.yith-wcms-pro ' + yith_wcms.dom.checkout_timeline + ' li').on('click', function (e) {

        var t = $(this);

        if (t.hasClass('active')) {
            return false;
        }

        var current_step    = $(yith_wcms.dom.checkout_timeline).find(yith_wcms.dom.active_timeline).data('step'),
            next_step       = t.data('step'),
            prev_step       = t.data('step') > current_step ? next_step - 1 : t.data('step'),
            action          = t.data('step') > current_step ? form_actions.find(yith_wcms.dom.button_next) : form_actions.find(yith_wcms.dom.button_prev);

        if (next_step == 0 && is_user_logged_in ) {
            return false;
        }

        change_step(action, current_step, next_step, prev_step);
    });

    // hide shipping error if checkbox not selected
    $('#ship-to-different-address-checkbox').on('change', function() {
        if ($(this).not(':checked')) {
            $('.woocommerce-shipping-fields .woocommerce-error').hide();
        } else {
        }
    });

    // handle forward/back buttons
    form_actions.find(yith_wcms.dom.button_prev).add(yith_wcms.dom.button_next).on( 'click', function(e){
        var t               = $(this),
            current_step    = form_actions.data('step'),
            next_step       = Number(current_step) + 1,
            prev_step       = Number(current_step) - 1;

        change_step(t, current_step, next_step, prev_step);

        // scroll to top of checkout field section if top is hidden
        if ($(window).scrollTop() > $('#checkout-wrapper').offset().top) {
                $('html, body').animate({
                    scrollTop: $("#checkout-wrapper").offset().top - 65
                }, 500);
        }
    });

    var change_step = function (t, current_step, next_step, prev_step) {

        var timeline    = $(yith_wcms.dom.checkout_timeline),
            action      = t.data('action'),
            prev        = form_actions.find(yith_wcms.dom.button_prev),
            next        = form_actions.find(yith_wcms.dom.button_next),
            active_step = timeline.find('.active').data('step');;

        var show_coupon = function( current_step ){
            // Summary and Payment tabs
            if (current_step == 4 || current_step == 5) {
                coupon.slideDown();
            } else {
                coupon.slideUp();
            }
        }

        // live fields validation
        if (yith_wcms.live_fields_validation == 'yes') {
            var checkout_form = $(yith_wcms.dom.checkout_form),
                invalid_field = 0,
                shipping_check = $(yith_wcms.dom.shipping_check);

            // inline validation
            checkout_form.find('.validate-required input:not([name="account_password"]), .validate-required select').trigger('change');
            if (! $('#ship-to-different-address-checkbox').is(':checked')) {
                shipping.find('.woocommerce-invalid').removeClass('woocommerce-invalid');
            }

            // billing
            if (active_step == 1) {
                invalid_field = billing.find(yith_wcms.dom.wc_invalid_required).size();
                if( create_account.length != 0 && ! create_account.is(':checked') && invalid_field != 0 ) {
                    invalid_field = invalid_field - create_account_wrapper.find('.validate-required').length;
                }
            }
            // shipping
            else if (active_step == 2 && shipping_check.is(':checked')) {
                invalid_field = shipping.find(yith_wcms.dom.wc_invalid_required).size();
            }

            // show errors and switch to first .fieldset with errors if necessary
            if (action == 'next' && (invalid_field != 0 || $('.woocommerce-invalid').length != 0)) {
                var firstInvalidStep = $('.woocommerce-invalid:first').parents('.fieldset').attr('class').match(/step-([0-9])/)[1]

                if ($('.woocommerce-billing-fields .woocommerce-error').length == 0) {
                    $('.woocommerce-billing-fields').find('h3:not(#source_heading_field)').after('<ul class="woocommerce-error custom"></ul>');
                    $('.woocommerce-error.custom').append('<li>Please correct the fields outlined in red below.</li>');
                } else if ($('.woocommerce-shipping-fields .woocommerce-error').length == 0) {
                    $('.woocommerce-shipping-fields').find('h3').after('<ul class="woocommerce-error custom"></ul>');
                    $('.woocommerce-error.custom').append('<li>Please correct the fields outlined in red below.</li>');
                } else {
                    $('.woocommerce-error.custom').show();
                }

                // navigate to first step with invalid fields
                form_actions.data('step', firstInvalidStep);
                steps[current_step].fadeOut('slow', function () {
                    steps[firstInvalidStep].fadeIn('slow');
                });

                $(yith_wcms.dom.active_timeline).toggleClass('active');
                $(yith_wcms.dom.timeline_id_prefix + firstInvalidStep).toggleClass('active');


                // stop execution
                return;
            } else {
                $('.woocommerce-error.custom').hide();
            }
        }

        timeline.find('.active').removeClass('active');

        if (action == 'next') {
            form_actions.data('step', next_step);
            steps[current_step].fadeOut('slow', function () {
                steps[next_step].fadeIn('slow');
                show_coupon(next_step);
            });

            $(yith_wcms.dom.timeline_id_prefix + next_step).toggleClass('active');
        }

        else if (action == 'prev') {
            form_actions.data('step', prev_step);
            steps[current_step].fadeOut('slow', function () {
                steps[prev_step].fadeIn('slow');
            });

            show_coupon(prev_step);
            $(yith_wcms.dom.timeline_id_prefix + prev_step).toggleClass('active');
        }

        current_step = form_actions.data('step');

        $.cookie(cookie.step, current_step, { path: '/' });

        // if current step is billing information and current user logged in or
        // current step is login and current user not logged in
        if (( current_step == 1 && is_user_logged_in == true ) ||
            ( is_user_logged_in == false && ( ( current_step == 0 && yith_wcms.checkout_login_reminder_enabled == 1 ) ||  ( current_step == 1 && yith_wcms.checkout_login_reminder_enabled == 0 ) ) )
        ) {
            prev.fadeOut('slow');
        }

        else {
            prev.fadeIn('slow');
        }

        // Last step
        if (current_step == 5) {
            next.fadeOut('slow');
            if (yith_wcms.disabled_prev_button == 'yes') {
                prev.fadeOut('slow');
            }
        }

        else {
            next.fadeIn('slow');
        }
    };

    var preset_form_value   = $.cookie(cookie.form),
        preset_current_step = $.cookie(cookie.step);

    var cache_form_value = function () {
        if (yith_wcms.dom.select2_fields.indexOf('source') == -1) {
            yith_wcms.dom.select2_fields.push('source');
        }
        $(yith_wcms.dom.checkout_form).find(yith_wcms.dom.required_fields_check).on('blur change', function (e) {
            var form_temp = $('.checkout.woocommerce-checkout').serialize();
            $.cookie(cookie.form, form_temp, { path: '/' } );
        });
    };

    var set_cached_value = function(preset_form_value, preset_current_step){
        if (typeof preset_form_value != 'undefined') {
            var form_temp = preset_form_value.split('&');
            for (var i in form_temp) {

                var elem = form_temp[i],
                    form_value = elem.split('='),
                    input_field = $(decodeURIComponent('input[name="' + form_value[0] + '"]'));

                if (typeof input_field != 'undefined') {
                    var cached_value = decodeURIComponent(form_value[1]).replace(/\+/g, ' ');
                    //Select2 Cached Value
                    if (yith_wcms.dom.select2_fields.indexOf(form_value[0]) != -1) {
                        var country_field = $('#' + decodeURIComponent(form_value[0]));
                        country_field.add(input_field).val(cached_value);
                        if (country_field.is('select')) {
                            country_field.val(cached_value).trigger('change');
                        }
                        else if (country_field.is('input')) {
                            input_field.val(cached_value);
                        }
                    }
                    else {
                        // skip cached value for nonce fields or other private WordPress fields
                        if( form_value[0].indexOf('_wp') == -1  ){
                            input_field.val(cached_value);
                        }
                    }
                }
            }
        }
    };

    cache_form_value();

    set_cached_value( preset_form_value, preset_current_step );

    $body.on('country_to_state_changed', function (e, value, obj) {
        cache_form_value();
    });

    if (typeof $.cookie(cookie.step) != 'undefined') {
        $body.on('updated_checkout yith_wcms_myaccount_order_pay', function(){
            $('.yith-wcms-pro ' + yith_wcms.dom.checkout_timeline + ' li#timeline-' + $.cookie(cookie.step)).trigger('click');
        });
    }

    // Delete cookie after order complete
    if( yith_wcms.is_order_received_endpoint == 1 ){
        $.removeCookie(cookie.form, { path: '/' })
        $.removeCookie(cookie.step, { path: '/' })
    }

    // save form data to cookie upon SO accept/decline actions
    $('#checkout-wrapper').on('click', 'a.button.addon', function() {
        Cookies.set('customer-info', $('form[name="checkout"]').serializeArray(), { expires: 7 });
    });

    // if cookie is set, update form data and skip to the offers tab
    if (Cookies.get('customer-info')) {
        $.each(JSON.parse(Cookies.get('customer-info')), function(index, array){
            var $el = $('[name="'+array.name+'"]'),
                $type = $($el).attr('type');

            if ($el.is('select')) {
                // <select>s
                var selectVal = array.value.trim();
                $el.find('option[value*="'+selectVal+'"]').attr('selected','selected');
            } else {
                // other inputs
                $el.val(array.value);
            }
        });

        // hide previously-completed steps
        $('#checkout_timeline .active').removeClass('active');
        $(steps[0]).fadeOut();
        $(steps[1]).fadeOut();

        // check if any more offers
        if ($('.woocommerce_offers').children().length == 1) {
            // only one element will be in the container if there are no offers available
            $('#checkout_timeline .order').addClass('active');
            $(steps[4]).fadeIn();
            form_actions.data('step', 4);
        } else {
            $('#checkout_timeline .smart-offer').addClass('active');
            $(steps[3]).fadeIn();
            form_actions.data('step', 3);
        }

    }

    // handle variations and add-to-cart buttons
    $('select[name="variation"]').each(setOfferVariation);
    $('select[name="variation"]').on('change', setOfferVariation);

    function setOfferVariation() {
        var thisProduct = $(this).attr('class'),
            thisVariation = $(this).val(),
            buyNowLink = $('a.'+thisProduct),
            newLink = buyNowLink.attr('href').replace(/add-to-cart=[\d]+/gi, 'add-to-cart=' + thisVariation);
        buyNowLink.attr('href', newLink);
        $('.discounted-price.' + thisProduct).html('$' + $(this).children('option:selected').data('discounted-price'));
        $('.full-price.' + thisProduct).html('$' + $(this).children('option:selected').data('full-price'));
    }

})(jQuery);
