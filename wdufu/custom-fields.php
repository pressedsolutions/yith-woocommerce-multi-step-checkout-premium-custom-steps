<?php
echo '<div class="wdufu_block"><div class="wdufu_title"><h3>'. __('How did you hear about us?').'</h3></div>';
if(isset($settings_arr['dvin_wdufu_options_to_display'])){
	$arr = array();
	$arr = explode("\n", $settings_arr['dvin_wdufu_options_to_display']);
	$final_arr = array();
	//loop through array and assign values as keys
	foreach($arr as $val) {
		$final_arr[$val] = $val;
	}
	$settings_arr['dvin_wdufu_option_type'] = isset($settings_arr['dvin_wdufu_option_type'])? 	$settings_arr['dvin_wdufu_option_type']:'Dropdown';
	if($settings_arr['dvin_wdufu_option_type'] == 'Dropdown') {
	$final_arr = array_merge(array('Select'=>__('Select','dvinwdufu')),$final_arr,array('Others'=>__('Others','dvinwdufu')));//push the Select option
		 woocommerce_form_field( 'wheredidyoufindus', array(
	    'type'  => 'select',
	    'class' => array('wdufuoptions_dropdown'),
	    'label' => __('Fill in this field'),
		'options' => $final_arr
	    ));
	} else {
		//push the others option
		$final_arr = array_merge($final_arr,array('Others'=>__('Others','dvinwdufu')));
		//loop through the array and display radio button
		foreach($final_arr as $key => $value){
			echo "<input type='radio' class='wdufuoptions_radio' name='wheredidyoufindus' value='$key'>&nbsp;".__($value,'dvinwdufu')."<br/>";
		}
		
	}
	 echo "<input type='text' name='wheredidyoufindus_others_textbox' id='wheredidyoufindus_others_textbox' value='' style='display:none;' placeholder='".__('Please specify','dvinwdufu')."'><br/>";
}
echo '</div>';