<?php
/*
Plugin Name: Yith Woocommerce Multi-Step Checkout Custom Steps
Plugin URI: https://bitbucket.org/pressedsolutions/yith-woocommerce-multi-step-checkout-premium-custom-steps
Description: Adds custom steps to Yith Woocommerce Multi-Step Checkout
Version: 1.3.7.11
Author: Andrew Minion/Pressed Solutions
Author URI: http://www.pressedsolutions.com
*/

const PLUGIN_VERSION = '1.3.7.11';

// check for parent plugin active status
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'yith-woocommerce-multi-step-checkout-premium/init.php' ) && ( 'yes' == get_option( 'yith_wcms_enable_multistep' ) ) ) {

    add_filter( 'yith_wcms_load_checkout_template_from_plugin', function() { return false; } );
    add_filter( 'woocommerce_locate_template', 'myplugin_woocommerce_locate_template', 15, 3 );
    function myplugin_woocommerce_locate_template( $template, $template_name, $template_path ) {
        global $woocommerce;
        $old_template = $template;

        if ( ! $template_path ) {
            $template_path = $woocommerce->template_url;
        }

        $plugin_path  = plugin_dir_path( __FILE__ ) . 'woocommerce/';

        // Look within passed path within the theme - this is priority
        $template = locate_template(
            array(
                $template_path . $template_name,
                $template_name
            )
        );
        // pull out extra 'woocommerce/' in template names
        $template_name = str_replace( 'woocommerce/', '', $template_name );

        // Get the template from this plugin, if it exists
        if ( ! $template && file_exists( $plugin_path . $template_name ) ) {
            $template = $plugin_path . $template_name;
        }

        // Use default template
        if ( ! $template ) {
            $template = $old_template;
        }

        // Return what we found
        return $template;
    }

    // load CSS
    add_action( 'wp_enqueue_scripts', 'ds_load_css' );
    function ds_load_css() {
        wp_register_style( 'yith-woocommerce-multistep-checkout-custom-css', plugins_url( 'multi-step-checkout.css', __FILE__ ), array(), PLUGIN_VERSION );

        if ( is_checkout() ) {
            wp_enqueue_style( 'yith-woocommerce-multistep-checkout-custom-css' );
        }
    }

    // load JS
    add_action( 'wp_enqueue_scripts', 'ds_load_modified_js', 10 );
    function ds_load_modified_js( $this ) {
        // load js.cookie
        wp_enqueue_script( 'js-cookie', plugins_url( 'js/js.cookie.min.js', __FILE__ ), NULL, '2.1.0', true );

        // load from theme instead of Yith plugin
        wp_dequeue_script( 'yith-wcms-step' );
        wp_enqueue_script( 'yith-wcms-step', plugins_url( 'js/multistep-premium-modified.min.js', __FILE__ ), array( 'wc-checkout', 'js-cookie' ), PLUGIN_VERSION, true );
    }

    // add new elements to JS DOM element
    add_filter( 'yith_wcms_frontend_dom_object', 'ds_new_screens' );
    function ds_new_screens( $array ) {
        $array['offers']        = '#checkout_offers';

        return $array;
    }

    // move "where did you hear" action
    add_action( 'init', 'ds_add_wdufu' );
    function ds_add_wdufu() {
        if ( class_exists( Dvin_Wdufu ) ) {
            $new_wdufu_obj = new Dvin_Wdufu();
            add_action( 'wheredidyouhear', array( $new_wdufu_obj, 'custom_add_checkout_fields' ) );

            global $wdufu_obj;
            remove_action( 'woocommerce_checkout_billing', array( $wdufu_obj, 'custom_add_checkout_fields' ) );
            remove_action( 'woocommerce_checkout_shipping', array( $wdufu_obj, 'custom_add_checkout_fields' ) );
            remove_action( 'woocommerce_before_order_notes', array( $wdufu_obj, 'custom_add_checkout_fields' ) );
            remove_action( 'woocommerce_after_order_notes', array( $wdufu_obj, 'custom_add_checkout_fields' ) );
        }
    }

    // remove "Order Notes" field
    add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields' );
    function alter_woocommerce_checkout_fields( $fields ) {
    unset($fields['order']['order_comments']);
    return $fields;
}

}

// add custom post type for upsell offers
function discounted_offer() {

    $labels = array(
        'name'                  => 'Upsell Offers',
        'singular_name'         => 'Upsell Offer',
        'menu_name'             => 'Upsell Offers',
        'name_admin_bar'        => 'Upsell Offer',
        'archives'              => 'upsell offer Archives',
        'parent_item_colon'     => 'Parent upsell offer:',
        'all_items'             => 'All Upsell Offers',
        'add_new_item'          => 'Add New Upsell Offer',
        'add_new'               => 'Add New',
        'new_item'              => 'New Upsell Offer',
        'edit_item'             => 'Edit Upsell Offer',
        'update_item'           => 'Update Upsell Offer',
        'view_item'             => 'View Upsell Offer',
        'search_items'          => 'Search Upsell Offers',
        'not_found'             => 'Not found',
        'not_found_in_trash'    => 'Not found in Trash',
        'featured_image'        => 'Featured Image',
        'set_featured_image'    => 'Set featured image',
        'remove_featured_image' => 'Remove featured image',
        'use_featured_image'    => 'Use as featured image',
        'insert_into_item'      => 'Insert into upsell offer',
        'uploaded_to_this_item' => 'Uploaded to this upsell offer',
        'items_list'            => 'Upsell offers list',
        'items_list_navigation' => 'Upsell offers list navigation',
        'filter_items_list'     => 'Filter upsell offers list',
    );
    $args = array(
        'label'                 => 'Upsell Offer',
        'description'           => 'Discounted Upsell Offers',
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 25,
        'menu_icon'             => 'dashicons-awards',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => false,
        'rewrite'               => false,
        'capability_type'       => 'page',
    );
    register_post_type( 'upsell_offer', $args );

}
add_action( 'init', 'discounted_offer', 0 );

// display upsell offers
add_action( 'show_upsell_offers', 'output_upsell_offers' );
function output_upsell_offers() {
    global $woocommerce;

    // get list of products in cart
    if ( $woocommerce->cart->cart_contents ) {
        $products_in_cart = array();
        foreach ( $woocommerce->cart->cart_contents as $product ) {
            $products_in_cart[] = $product['product_id'];
        }
    }

    // WP_Query arguments
    $args = array (
        'post_type'              => array( 'upsell_offer' ),
        'posts_per_page'         => '-1',
        'meta_query'             => array(
            array(
                'key'       => 'product',
                'value'     => $products_in_cart,
                'compare'   => 'NOT IN',
                'type'      => 'NUMERIC',
            ),
        ),
    );

    // The Query
    $upsell_query = new WP_Query( $args );

    // The Loop
    if ( $upsell_query->have_posts() ) {
        echo '<h1>Additional Offers</h1>';
        while ( $upsell_query->have_posts() ) {
            $upsell_query->the_post();

            // check equivalent_products field
            $equivalent_products = get_field( 'equivalent_products' );
            $equivalent_products_array = array();
            if ( $equivalent_products ) {
                foreach ( $equivalent_products as $this_product ) {
                    $equivalent_products[] = $equivalent_products->ID;
                }
                if ( array_intersect( $products_in_cart , $equivalent_products_array ) ) {
                    continue;
                }
            }

            // get WooCommerce product
            $product_id = get_field( 'product' )->ID;
            $product = wc_get_product( $product_id );
            if ( 'variable' == $product->product_type ) {
                $variation_array = $product->get_available_variations();
            }

            // output offer
            echo '<div class="upsell clearfix">';
            echo '<h2 class="entry-title">' . get_the_title();
            if ( has_post_thumbnail() ) {
                the_post_thumbnail( array( 150, 200 ), array( 'class' => 'alignright' ) );
            }
            echo '</h2>';
            the_content();

            // set up variation dropdown
            if ( $variation_array ) {
                $variations_options = NULL;
                foreach ( $variation_array as $this_variation ) {
                    if ( $this_variation['is_in_stock'] ) {
                        $variation_object = wc_get_product( $this_variation['variation_id'] );
                        $discount_amount = get_field( 'discount_amount', $_GET['offer_id'] );
                        $discounted_price = ( $this_variation['display_price'] * ( 1 - ( .01 * $discount_amount ) ) );

                        $variations_options .= '<option value="' . $this_variation['variation_id'] . '" data-full-price="' . number_format( $this_variation['display_price'], 2 ). '" data-discounted-price="' . number_format( round( $discounted_price, 2 ), 2 ) . '">' . $variation_object->get_formatted_variation_attributes( true ). '</option>';
                    }
                }
                echo '<p><select name="variation" class="' . $product_id . '">' . $variations_options . '</select> <del class="' . $product_id . ' full-price">$</del> <span class="' . $product_id . ' discounted-price"></span></p>';
            }

            echo '<p><a href="' . home_url() . '/checkout/?source=checkout-offer&offer_id=' . get_the_ID() . '&add-to-cart=' . $product_id . '" class="button alt addon ' . $product_id . '">Add to Cart</a></p>';
            echo '</div>';
        }

        // redirect to checkout page when adding to cart
        function custom_add_to_cart_redirect() {
            return home_url( '/checkout/' );
        }
        add_filter( 'woocommerce_add_to_cart_redirect', 'custom_add_to_cart_redirect' );

    } else {
        echo '<h2>No offers currently available</h2>';
    }

    // Restore original Post Data
    wp_reset_postdata();
}

// add ACF fields
if ( function_exists("register_field_group") ) {
    register_field_group(array (
        'id' => 'acf_upsell-offers',
        'title' => 'Upsell Offers',
        'fields' => array (
            array (
                'key' => 'field_572b994732e8b',
                'label' => 'Product',
                'name' => 'product',
                'type' => 'post_object',
                'instructions' => 'Choose the product to offer at a discount',
                'required' => 1,
                'post_type' => array (
                    0 => 'product',
                ),
                'taxonomy' => array (
                    0 => 'all',
                ),
                'allow_null' => 0,
                'multiple' => 0,
            ),
            array (
                'key' => 'field_572b996d32e8c',
                'label' => 'Discount Amount',
                'name' => 'discount_amount',
                'type' => 'number',
                'instructions' => 'Choose the percentage to discount the product.',
                'required' => 1,
                'default_value' => 10,
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'min' => 0,
                'max' => 100,
                'step' => 1,
            ),
            array (
                'key' => 'field_5751b1b5ef45c',
                'label' => 'Equivalent Products',
                'name' => 'equivalent_products',
                'type' => 'post_object',
                'instructions' => 'Choose any other products that are equivalent to this and should prevent this upsell from being offered if they are in the cart.',
                'post_type' => array (
                    0 => 'product',
                ),
                'taxonomy' => array (
                    0 => 'all',
                ),
                'allow_null' => 0,
                'multiple' => 1,
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'upsell_offer',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'acf_after_title',
            'layout' => 'default',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

// add item meta to accepted upsell offers
add_filter( 'woocommerce_add_cart_item_data', 'ywcms_add_cart_item_data', 10, 2 );
add_filter( 'woocommerce_get_cart_item_from_session', 'ywcms_get_cart_item_from_session', 5, 3 );
function ywcms_add_cart_item_data( $cart_item_meta, $product_id ) {
    global $woocommerce;
    if ( 'checkout-offer' == $_GET['source'] ) {
        $cart_item_meta['source'] = 'checkout-offer';
    }
    return $cart_item_meta;
}
function ywcms_get_cart_item_from_session( $item, $values, $key ) {
    if ( array_key_exists( 'source', $item ) ) {
        $item['source'] == $values['source'];
    }
    return $item;
}

// adjust price when added to the cart
add_filter( 'woocommerce_add_cart_item', 'ywcms_set_prices' );
add_filter( 'woocommerce_get_cart_item_from_session', 'ywcms_set_session_prices', 20, 3 );

function ywcms_set_prices( $woo_data ) {
    $discount_amount = get_field( 'discount_amount', $_GET['offer_id'] );
    $discounted_price = ( $woo_data['data']->get_price() * ( 1 - ( .01 * $discount_amount ) ) );
    if ( 'checkout-offer' == $woo_data['source'] ) {
        $woo_data['data']->set_price( $discounted_price );
        $woo_data['upsell_price'] = $discounted_price;
        $woo_data['upsell_offer'] = $_GET['offer_id'];

        function custom_add_to_cart_redirect() {
            return home_url( '/checkout/' );
        }
        add_filter( 'woocommerce_add_to_cart_redirect', 'custom_add_to_cart_redirect' );
    }
    return $woo_data;
}

function ywcms_set_session_prices( $woo_data, $values, $key ) {
    if ( isset( $woo_data['upsell_price'] ) || ! empty( $woo_data['upsell_price'] ) ) {
        $woo_data['data']->set_price( $woo_data['upsell_price'] );
    }

    return $woo_data;
}
